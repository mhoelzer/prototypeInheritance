function Speaker(options) {
    this.givenName = options.givenName;
    this.surname = options.surname;
    this.email = options.email;
    this.biography = options.biography;
    this.isActive = true;
}

Speaker.prototype.getBiography = function() {
    console.log(`Name: ${this.givenName} ${this.surname}, Bio: ${this.biography}`);
}

Speaker.prototype.markInactive = function(date) {
    this.isActive = false;
    this.inactiveDate = date;
}

function KeynoteSpeaker(options) {
    Speaker.call(this, options);
    this.websites = options.websites;
    this.keynoteTopics = options.keynoteTopics;
    this.breakouts = options.breakouts;
}

KeynoteSpeaker.prototype = Object.create(Speaker.prototype);
KeynoteSpeaker.prototype.constructor = KeynoteSpeaker;

function WorkshopSpeaker(options) {
    Speaker.call(this, options);
    this.workshopTopics = options.workshopTopics;
}
WorkshopSpeaker.prototype = Object.create(Speaker.prototype);
WorkshopSpeaker.prototype.constructor = WorkshopSpeaker;

const firstSpeaker = new Speaker({
    givenName: "Burt", 
    surname: "McGee", 
    email: "bmg@kenzie.org", 
    biography: "MurderBird"
});

const firstKeynoteSpeaker = new KeynoteSpeaker({
    givenName: "Bob",
    surname: "Boberson",
    email: "bobboberson@hotmail.com",
    biography: "Boberson Idol",
    websites: ["bobtheboberson.com", "bobtheboberson53.com"],
    keynoteTopics: ["Talking About Bob", "Bob the Boberson"],
    breakouts: ["Bob's Early Life", "Bob Bob Bob Bob Bob Bara Anne", "Dealing with Bob"]        
});

const workshopSpeaker = new WorkshopSpeaker({
    givenName: "Glass",
    surname: "Butterfly",
    email: "glassButt@gmail.com",
    biography: "Butterfly made of glass",
    workshopTopics: ["What Makes a Butterfly Glass?", "How to Maintain Yo Glass", "Watch Your Glass"]
});

// undefined is replacer to alter things ; 4 is number of spaces to be inserted at beginning of line
console.log("workshopSpeaker:\n" + JSON.stringify(workshopSpeaker, null, 4));
console.log("KeynoteSpeaker:\n" + JSON.stringify(firstKeynoteSpeaker, null, 4));
console.log("firstSpeaker: \n" + JSON.stringify(firstSpeaker, null, 4));


firstSpeaker.markInactive(new Date('2019-6-12'));
console.log("Apparently firstSpeaker feels entitled to a vacation...");
console.log("firstSpeaker: \n" + JSON.stringify(firstSpeaker, null, 4));
firstSpeaker.getBiography();

firstKeynoteSpeaker.markInactive(new Date("May 6, 1234"));
console.log("firstKeynoteSpeaker abandons the keynote speech")
console.log("KeynoteSpeaker:\n" + JSON.stringify(firstKeynoteSpeaker, null, 4));
firstKeynoteSpeaker.getBiography();